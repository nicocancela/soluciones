
public class Clase31_08 {
	// Ejercicios recursivos sobre Strings y Arreglos
	public static void main(String[] args) {
		imprimir("Juan"); // Juan --> //J u a n
		System.out.println(inverso("Juan"));
		int[] miVector = { 1, 2, 3 };
		imprimir(miVector);
		System.out.println(combinar("fgd", "adfxgtgs"));
		System.out.println(esAbecedaria(""));
		System.out.println(pertenece(miVector, 2));
	}

	// Escribir una función recursiva void imprimirEspaciado que tome una cadena
	// e imprima los caracteres de la misma con un espacio entre cada uno de ellos.
	// Ejemplo: imprimirEspaciado("Juan") imprime en pantalla: J u a n
	public static void imprimir(String s) {
		if (s.isEmpty()) {
			return;
		} else {
			System.out.print(s.charAt(0) + " ");
			imprimir(resto(s));
		}
	}

	// Escribir una función recursiva llamada String inverso(String s) que tome
	// una cadena s y devuelva otra que contenga los caracteres de s en orden
	// inverso.
	public static String inverso(String s) {
		if (s.isEmpty()) {
			return "";
		} else {
			return inverso(resto(s)) + s.charAt(0);
		}
	}

	// void imprimir(int[] a): Imprime por pantalla los elementos del arreglo.
	public static void imprimir(int[] vector) {
		if (vector.length == 0) {
			return;
		} else {
			System.out.print(vector[0] + ", ");
			imprimir(resto(vector));
		}
	}

	// int suma(int[] a): Hacer una función recursiva que sume los elementos de un
	// arreglo
	public static int suma(int[] a) {
		if (a.length == 0) {
			return 0;
		}
		return a[0] + suma(resto(a));
	}

	// Escribir una función recursiva String combinar(String s1, String s2) que
	// tome dos cadenas y devuelva otra que resulte de comparar los strings caracter
	// a caracter y colocar el menor de ellos en el resultado. Si tienen longitudes
	// distintas, se deja el resto del string que quede sin comparar.
	/*
	 * Ejemplo: String res = combinar("abd", "bbc") //res vale "abc" String res =
	 * combinar("fgd", "adfxgtgs") //res vale "addxgtgs"
	 */
	public static String combinar(String s1, String s2) {
		if (s1.isEmpty()) {
			return s2;
		}
		if (s2.isEmpty()) {
			return s1;
		}
		if (s1.charAt(0) < s2.charAt(0)) {
			return s1.charAt(0) + combinar(resto(s1), resto(s2));
		} else {
			return s2.charAt(0) + combinar(resto(s1), resto(s2));
		}
	}

	// Implementar el método boolean esAbecedaria(String s) que devuelve true
	// si s es una palabra abecedaria o false en caso contrario.
	public static boolean esAbecedaria(String s) {
		if (s.length() <= 1) {
			return true;
		}
		if (s.charAt(0) > s.charAt(1)) {
			return false;
		} else {
			return esAbecedaria(resto(s));
		}
	}

	// boolean pertenece(int[] a, int n): Dado un arreglo, hacer una función que
	// devuelva si un número está en el arreglo o no.
	public static boolean pertenece(int[] a, int n) {
		if (a.length == 0) {
			return false;
		}
		if (a[0] == n) {
			return true;
		}
		return pertenece(resto(a), n);
	}

	public static String resto(String s) {
		String ret = "";
		for (int i = 1; i < s.length(); i++) {
			ret = ret + s.charAt(i);
		}
		return ret;
	}

	public static int[] resto(int[] arreglo) {
		if (arreglo.length == 0) {
			int[] ret = {};
			return ret;
		}
		int[] aux = new int[arreglo.length - 1];
		for (int i = 1; i < arreglo.length; i++) {
			aux[i - 1] = arreglo[i];
		}
		return aux;
	}
}
