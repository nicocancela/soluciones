public class Fecha {
	int dia;
	int mes;
	int año;

	public Fecha(int dia, int mes, int año) {
		this.dia = dia;
		this.mes = mes;
		this.año = año;
	}
	
	public int diasDelMes(int mes, int año) {
		if(mes<1 || mes>12) {
			throw new RuntimeException("La fecha ingresada es inválida");
		}
		if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
			return 30;
		}
		if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
			return 31;
		}
		if (mes == 2 && esBisiesto(año)) { // Si el mes es Febrero me fijo si el año es bisiesto.
			return 29;
		}
		return 28;
	}
	
	// Un año es bisiesto si:
	// Es divisible entre 4 y no es divisible entre 100.
	// Es divisible entre 100 y 400.
	public static boolean esBisiesto(int a) {
		if (a % 4 == 0 && a % 100 != 0) {
			return true;
		} else if (a % 400 == 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean esBisiesto() {
		int a = this.año;
		if (a % 4 == 0 && a % 100 != 0) {
			return true;
		} else if (a % 400 == 0) {
			return true;
		} else {
			return false;
		}
	}	

	public void avanzarDia() {
		if (dia == diasDelMes(mes, año)) {
			this.dia = 1;
			if (mes == 12) {
				this.mes = 1;
				this.año = this.año + 1;
			} else {
				this.mes = this.mes + 1;
			}
		} else {
			this.dia = this.dia + 1;
		}
	}

	public boolean esValida() {
		boolean diaValido = this.dia >= 1 && this.dia <= diasDelMes(this.mes, this.año);
		boolean mesValido = this.mes >= 1 && this.mes <= 12;
		boolean añoValido = true;
		return diaValido && mesValido && añoValido;
	}

	//27/02/2021 --> 27/02/2021
	public boolean antesQue(Fecha otraFecha) {
		if (this.año < otraFecha.año) {
			return true;
		} else {
			if (this.año == otraFecha.año) {
				if (this.mes < otraFecha.mes)
					return true;
				else
					return this.mes == otraFecha.mes && this.dia < otraFecha.dia;
			}
		}
		return false;
	}

	//23/08/2020
	public int diaDelAño() {
		int diasAño = 0;
		for (int i = 1; i < this.mes; i++) {
			diasAño = diasAño + diasDelMes(i, this.año);
		}
		diasAño = diasAño + dia;
		return diasAño;
	}

	//this = 22/03/2021 --> masChica
	//otra = 18/08/2021 --> masGrande
	public int diasDeDiferenciaCon(Fecha otra) {
		// Forma sencilla - modifica las variables
		// Encontramos la fecha mas chiquita
		Fecha masChica = null;
		Fecha masGrande = null;

		if (this.antesQue(otra)) {
			masChica = this;
			masGrande = otra;
		} else {
			masChica = otra;
			masGrande = this;
		}
		// Vamos avanzando días hasta que la fecha más chiquita sea igual a la más
		// grande
		int contador = 0;
		while (masChica.antesQue(masGrande)) {
			masChica.avanzarDia();
			contador++;
		}
		return contador;
	}

	public void imprimirFecha() {
		System.out.println(dia + "/" + mes + "/" + año);
	}
}
