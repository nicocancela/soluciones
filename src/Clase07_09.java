
public class Clase07_09 {// Ejercicios Parciales viejos

		public static void main(String args[]) {
			System.out.println(tomarCaracteresDesde("hola", 0, 2));
			System.out.println(tomarCaracteresDesde("hola", 0, 9));
			System.out.println(alternaVocales("ropero", "mula"));
			System.out.println(alternaVocales("ropero", "mulata"));
			System.out.println(alternaVocales("ropero", ""));
			System.out.println(alternaVocales("ropa", "muleta"));
			System.out.println(alternaVocales("", "algo"));
		}

		 /* 1- Escribir una función recursiva String tomarCaracteresDesde(String s, int
		 * desde, int cant) que toma un String s, una posición desde, una cantidad cant
		 * y devuelve una nueva cadena que resulta de tomar cant caracteres de s desde
		 * la posición desde. Por ejemplo: tomarCaracteresDesdeHasta("sobrenatural",0,5)
		 * puede devolver "sobre". tomarCaracteresDesdeHasta("capa",7,10) puede devolver
		 * "". tomarCaracteresDesdeHasta("claramente",2,3) puede devolver "ara".
		 * tomarCaracteresDesdeHasta("programacion",4,4) puede devolver "rama".
		 * tomarCaracteresDesdeHasta("domingo",2,10) puede devolver "mingo".
		 * tomarCaracteresDesdeHasta("",1,5) puede devolver "".*/
		
		public static String tomarCaracteresDesde(String s, int desde, int cant) {
			if (s.length() == 0) {
				return "";
			}
			if (desde > 0) {
				return tomarCaracteresDesde(resto(s), desde - 1, cant);
			}
			if (cant > 0) {
				return s.charAt(0) + tomarCaracteresDesde(resto(s), desde, cant - 1);
			}
			return "";
		}


		 /* 2- Escribir la función recursiva public static String alternaVocales(String
		 * s,String t) que devuelve la cadena que se obtiene al cambiar las vocales de
		 * la cadena s por las vocales de la cadena t a medida que van apareciendo de
		 * izquierda a derecha. Si hay menos vocales en t entonces las vocales de s que
		 * siguen no cambian. Por ejemplo: alternaVocales("ropero", "mula") debe
		 * devolver "ruparo". alternaVocales("ropero", "mulata") debe devolver "rupara".
		 * alternaVocales("ropa", "muleta") debe devolver "rupe".
		 * alternaVocales("ropero","") debe devolver "ropero". alternaVocales("",
		 * "algo") debe devolver "".*/
		
		public static String alternaVocales(String s, String t) {
			if (s.length() == 0) {
				return s;
			}
			if (t.length() == 0) {
				return s;
			}
			// CASO: s es vocal, y t es vocal --> PERMUTO.
			if (esVocal(s.charAt(0)) && esVocal(t.charAt(0))) {
				return t.charAt(0) + alternaVocales(resto(s), resto(t));
			}

			// CASO: s es vocal, y t no vocal --> ESPERO HASTA ENCONTRAR VOCAL EN t.
			if (esVocal(s.charAt(0)) && !esVocal(t.charAt(0))) {
				return alternaVocales(s, resto(t));
			}
			// CASO: s no es vocal, y t es vocal --> AVANZO EN S, PERO NO EN T.
			if (!esVocal(s.charAt(0)) && esVocal(t.charAt(0))) {
				return s.charAt(0) + alternaVocales(resto(s), t);
			}

			// CASO: s no es vocal, y t no es vocal --> AVANZO EN S y T.
			return s.charAt(0) + alternaVocales(resto(s), resto(t));
		}
		
		public static boolean esVocal(char letra) {
			return letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u';
		}

		public static String resto(String s) {
			String ret = "";
			for (int i = 1; i < s.length(); i++) {
				ret = ret + s.charAt(i);
			}
			return ret;
		}
	}