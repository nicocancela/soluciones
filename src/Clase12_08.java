
public class Clase12_08 {

	public static void main(String[] args) {
		// Ej: Cantidad de divisores
		System.out.println("Cantidad divisores de 45: " + cantDivisores(45));
		System.out.println("Cantidad divisores de 2374: " + cantDivisores(2374));
		System.out.println("Cantidad divisores de 3529: " + cantDivisores(3529));
		// Ej: Siguiente primo
		System.out.println("Siguiente primo de 8: " + siguientePrimo(8));
		System.out.println("Siguiente primo de 230: " + siguientePrimo(230));
		System.out.println("Siguiente primo de 3527: " + siguientePrimo(3527));
		// Ej: Factorial
		System.out.println("Factorial de 3: " + factorial(3));
		System.out.println("Factorial de 7: " + factorial(7));
		System.out.println("Factorial de 16: " + factorial(16));

	}

	public static int cantDivisores(int numero) {
		int cantidadDivisores = 0;
		for (int i = 1; i <= numero; i++) {
			if (numero % i == 0) {
				cantidadDivisores++;
			}
		}
		return cantidadDivisores;
	}

	public static boolean esPrimo(int numero) {
		return cantDivisores(numero) == 2;
	}

	public static int siguientePrimo(int numero) {
		int numeroActual = numero + 1;
		while (!esPrimo(numeroActual)) {
			numeroActual++;
		}
		return numeroActual;
	}

	public static int factorial(int numero) {
		int factorialRet = 1;
		for (int i = 1; i <= numero; i++) {
			factorialRet = factorialRet * i;
		}
		return factorialRet;
	}

}
